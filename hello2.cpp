///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07c - My First Cat
///
/// @file hello2.cpp
/// @version 1.0
///
/// Program 2 of My First Cat
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
/// @date   Feb 24 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {
    
    std:: cout << "Hello World!" << std:: endl;

}

