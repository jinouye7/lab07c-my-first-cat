///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07c - My First Cat
///
/// @file hello3.cpp
/// @version 1.0
///
/// Program 3 of My First Cat
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
/// @date   Feb 24 2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

class Cat {
public:
   void sayHello() {
      std:: cout << "Meow" << std:: endl;
   } 
} ;


int main() {
    
    Cat myCat;

    myCat.sayHello();

}

