///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07c - My First Cat
///
/// @file hello1.cpp
/// @version 1.0
///
/// Program 1 of My First Cat
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
/// @date   24_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <iostream>

using namespace std;

int main() {

    cout << "Hello World!" << endl; 

}

